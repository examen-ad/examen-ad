<?php

namespace app\controllers;

use app\models\Ciclista;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use \app\models\Puerto;
use \app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */


    // ...

    public function actionIndex()
    {
        // Consulta 1
        $ciclistaQuery = Ciclista::find()->where(['between', 'edad', 25, 35]);
        $dataProviderConsulta1 = new \yii\data\ActiveDataProvider([
            'query' => $ciclistaQuery,
        ]);

        // Consulta 2
        $etapaQuery = Etapa::find()->select(['numetapa', 'kms']);
        $dataProviderConsulta2 = new \yii\data\ActiveDataProvider([
            'query' => $etapaQuery,
        ]);

        // Consulta 3
        $puertoQuery = Puerto::find()->where(['>', 'altura', 1500]);
        $dataProviderConsulta3 = new \yii\data\ActiveDataProvider([
            'query' => $puertoQuery,
        ]);



        return $this->render('index', [
            'dataProviderConsulta1' => $dataProviderConsulta1,
            'dataProviderConsulta2' => $dataProviderConsulta2, // Agregando el nuevo proveedor de datos
            'dataProviderConsulta3' => $dataProviderConsulta3,
        ]);
    }





    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

}