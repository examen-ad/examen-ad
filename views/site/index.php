<?php
use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */
$this->title = 'Aplicación Yiis';
?>

<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <!-- Consulta 1 -->
            <div class="card-body tarjeta">
                <h3>Consulta 1</h3>
                <p>Los ciclistas cuya edad está entre 25 y 35 años mostrando únicamente el dorsal y el nombre
                    del ciclista</p>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderConsulta1,
                    'columns' => [
                        'dorsal',
                        'nombre',
                    ],
                    'options' => ['class' => 'table-responsive table table-striped table-dark'],
                ]); ?>

            </div>
        </div>

        <div class="row">
            <!-- Consulta 2 -->
            <div class="card-body tarjeta">
                <h3>Consulta 2</h3>
                <p>Las etapa no circulares mostrando sólo el número de etapa y la longitud de las mismas</p>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderConsulta2,
                    'columns' => [
                        'numetapa',
                        'kms',
                    ],
                    'options' => ['class' => 'table-responsive table table-striped table-dark'],
                ]); ?>

            </div>
        </div>

        <!-- Consulta 3 -->
        <div class="row">
            <div class="card-body tarjeta">
                <h3>Consulta 3</h3>
                <p>Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del
                    ciclista que lo ganó.</p>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderConsulta3,
                    'columns' => [
                        'nompuerto',
                        'dorsal',
                    ],
                    'options' => ['class' => 'table-responsive table table-striped table-dark'],
                ]); ?>

            </div>
        </div>
    </div>